Server's job:
- parse specified logfiles, share them out
- auth - only share to specific identities

-> what logfiles?
-> how to parse them?
-> cache?
-> auth?
-> who can see which logfiles?
-> store/serve out layouts?

Client's job:
- web interface
- pull in data from server
- show specific reports
- realtime?


#1 - simplest:
Server - Spring webapp - on request:
- read specific file, serve it as json

Client - angular app - on refresh:
- request data from server, draw specific chart


