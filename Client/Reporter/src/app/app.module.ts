import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { GoogleChartsModule } from 'angular-google-charts';

import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ReporterComponent } from './reporter/reporter.component';

@NgModule({
  declarations: [
    AppComponent,
    ReporterComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    GoogleChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
