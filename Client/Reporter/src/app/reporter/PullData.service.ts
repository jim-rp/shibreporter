import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable ({providedIn: 'root'})
export class PullDataService {

  constructor(private http: HttpClient) {}

  getSPCountOverTime() : Observable <any> {
    return this.http.get<any>("/timeBySPCount", {withCredentials : true });
  }
}

/*
export interface LogEntry {
  timestamp: string,
  urn: string,
  sessionID: string,
  sp: string,
  profile: string,
  idp: string
}



@Injectable({
  providedIn: 'root'
})
export class PullDataService {

  constructor(private http: HttpClient) {}



  getSalesOverTimeAndState() : Observable <any> {
    return this.http.get<any>("/orderData", {withCredentials : true })
      .pipe(
      );
  }
}
*/
