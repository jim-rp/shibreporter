import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { PullDataService } from './PullData.service';
import { ChartBase, ChartEditorComponent, ChartType, FilterType, Row } from 'angular-google-charts';
import { ChartData } from './ChartData';



@Component({
  selector: 'app-reporter',
  templateUrl: './reporter.component.html',
  styleUrls: ['./reporter.component.css']
})
export class ReporterComponent implements OnInit {

  filterType = FilterType.ChartRange;
  options = {'filterColumnIndex': 2};
  /*
  chartData : ChartData = {
    "title": "Stuff",
    "type": ChartType.Bar,
    "data": [
      ["116-0",1,1,1],
      ["116-1",1,1,1],
      ["116-10",1,1,1],
      ["116-11",1,1,1],["116-2",1,1,1],
      ["116-3",1,1,1],["116-4",1,1,1],
      ["116-5",1,1,1],["116-6",1,1,1],
      ["116-7",1,1,1],["116-8",1,1,1],
      ["116-9",1,1,1],["117-0",1,1,1],
      ["117-1",1,1,1],["117-10",1,1,1],
      ["117-11",1,1,1],["117-2",1,1,1],
      ["117-3",1,1,1],["117-4",1,1,1],
      ["117-5",1,1,1],["117-6",1,1,1],
      ["117-7",1,1,1],["117-8",1,1,1],
      ["117-9",1,1,1],["118-0",1,1,1],
      ["118-1",1,1,1],["118-10",0,1,1],
      ["118-11",0,0,1],["118-2",1,1,1],
      ["118-3",1,1,1],["118-5",0,1,0],
      ["118-6",1,0,1],["118-7",0,1,0],
      ["118-8",0,1,0],["118-9",1,0,0]
    ], "columnNames": ["date","CA","NY","TX"]
    , "options": ""};
*/

  @ViewChild(ChartEditorComponent)
  public readonly editor: ChartEditorComponent;

  public editChart(chart: ChartBase) {
    this.editor
      .editChart(chart)
      .afterClosed()
      .subscribe(result => {
        if (result) {
          // Saved
        } else {
          // Cancelled
        }
      });
  }
  chartData: ChartData;
  
  chartInfo = {
    "title": "things",
    "type": ChartType.Bar,
    "options": ""
  };

  constructor(private data: PullDataService) {
    let thing = this.data.getSPCountOverTime();

    thing.subscribe(
      result => {
        this.chartData = result;
      }
    );

  }

//  constructor() { }

  ngOnInit(): void {
  }


}
