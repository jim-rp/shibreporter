package mash.pies.reporting.server.chartUtils;

/**
 * Key represents a 2 dimensional key for a lookup table to store chart source data
 * 
 */
public class Key {

    private final String primary;
    private final String secondary;

    public Key (String primary, String secondary) {
        this.primary = primary;
        this.secondary = secondary;
    }

    @Override
    public boolean equals (Object o) {
        if (!o.getClass().equals(this.getClass())) 
            return false;
        
        Key k = (Key) o;

        if (k.primary.equals(this.primary) && k.secondary.equals(this.secondary))
            return true;
        else
            return false;
    }

    @Override
    public String toString () {
        return primary+":"+secondary;
    }
    
    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    public String getPrimary() {
        return primary;
    }

    public String getSecondary() {
        return secondary;
    }
}