package mash.pies.reporting.server.chartUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import com.google.gson.JsonArray;

/**
 * 2D dimensional dataset for export to google charts.
 * 
 * no constraint on dimension count here, expect it will get ugly if you add data with varying number of dimensions...
 */
public class DataSet <T> { // Object often a good choice here.... failing that, Number/Integer/Float...

    private Map <Key, T> data = new HashMap <Key, T> ();

    public DataSet () {
    }

    /**
     * retrieves value for given key 
     * 
     * tip: to add (currently) empty columns/rows, add a key with a null - eg new Key("stuff", null) or new Key (null", "2021-05-03")
     */
    public T get (Key key) {
        return data.get (key);
    }

    /**
     * sets value for given key
     */
    public void set (Key key, T value) {
        data.put(key, value);
    }

    /**
     * retrieves list of all keys
     * @return
     */
    public Set <Key> getKeys() {

        return data.keySet();
    }

    /**
     * retrieves an alphabetically/numerically sorted list of primary keys from all keys currently used within the dataset
     * @param dimension
     * @return
     */
    public SortedSet <String> getPrimaries () {

        SortedSet <String> result = new TreeSet <String> ();

        for (Key k : data.keySet())
            result.add(k.getPrimary());

        return result;
    }

        /**
     * retrieves an alphabetically/numerically sorted list of secondary keys from all keys currently used within the dataset
     * @param dimension
     * @return
     */
    public SortedSet <String> getSecondaries () {

        SortedSet <String> result = new TreeSet <String> ();

        for (Key k : data.keySet())
            result.add(k.getSecondary());

        return result;
    }

    /**
     * returns a list of 
     * @param primary
     * @return
     */
    public List<T> getByPrimary (String primary) {

        List <T> result = new ArrayList <T> ();

        Iterator <String> pIter = getSecondaries().iterator();
        while (pIter.hasNext()) {
            Key k = new Key (primary, pIter.next());
            T val = data.get(k);
            result.add(val);
        }
            
        return result;
    }

    // JSON bits:

    /**
     * resturns a Json array of the secondary key values
     * @return
     */
    public JsonArray getHeaders () {

        JsonArray result = new JsonArray ();
        result.add("index");
        for (String val : getSecondaries())
            result.add(val);
        return result;
    }

    private JsonArray getRowAsJson (String primary, T fillValue) {
        JsonArray result = new JsonArray ();
        // each row starts with primary key
        result.add(primary);
        
        for (T value : getByPrimary(primary)) {
            if (value == null)
                value = fillValue; 
            if (value instanceof Number)
                result.add((Number) value);
            else
            result.add(value.toString());
        }
        return result;
    }

    public JsonArray getData (T fillValue) {
        JsonArray result = new JsonArray ();
        
        for (String primary : getPrimaries())
            result.add(getRowAsJson(primary, fillValue));
        
        return result;
    }   
}
