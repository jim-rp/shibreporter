package mash.pies.reporting.server;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import mash.pies.reporting.server.LogRepository.LogEntry;
import mash.pies.reporting.server.chartUtils.CountDataSet;
import mash.pies.reporting.server.chartUtils.Key;

@RestController
public class LogController {

    @Autowired
    private LogParserService parser;

    @Autowired LogRepository logRepo;

    @GetMapping("/readLogs")
    public int readLogs () throws IOException {
        return parser.importLogs(false);
    }

    @GetMapping("/timeBySPCount")
    public String getTimeBySPCount () {
        
        CountDataSet dataSet = new CountDataSet ();

        JsonObject result = new JsonObject ();
        Iterator <LogEntry> logIter = logRepo.findAll().iterator();
        while (logIter.hasNext()) {
            LogEntry e = logIter.next();
            Key key = new Key (e.getTimeStamp().substring(0, 6), e.getServiceProvider());
            dataSet.increment(key);
        }

        result.add("columnNames",dataSet.getHeaders());
        result.add("data", dataSet.getData());

        return result.toString();
    }

}