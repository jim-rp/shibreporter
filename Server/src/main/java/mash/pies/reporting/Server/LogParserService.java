package mash.pies.reporting.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.HashSet;
import java.util.zip.GZIPInputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import mash.pies.reporting.server.LogRepository.LogEntry;

/**
 * Reads audit files into the database.
 * 
 */
@Service
/// doesnt work dont know why @ConfigurationProperties(prefix="mash.pies.reporting", ignoreUnknownFields = false)
public class LogParserService {

    @Autowired
    private LogRepository logRepo;

//    @Value("${log-directories}")
    private String [] logDirectories = new String [] {"c:/users/jim/downloads/idp/"};

//  @Value("${log-file-patterns"})
    private String [] logPatterns = new String [] {"idp-audit.log", "idp-audit-*.log.gz"};
  
    


//    @Value("${mash.pies.reporting.Server.LogParserService.logFilePatterns}")
//    private String [] logFilePatterns;

    //    private static String[] fields = { "srcIp", null, "timestamp", "username", "sp", "sessionID", null, "initialAuthTime", "attributes", null, null, null, null, "action", null, null, null, "os"};

    public LogParserService() {}

    /**
     * 
     * 
     * @param logfile
     * @return number of new entries added
     * @throws IOException
     */
    public int readLogfile (String logfile) throws IOException {

        FileInputStream fis = new FileInputStream (logfile);
        BufferedReader br;
        if (logfile.endsWith("gz"))
            br = new BufferedReader(new InputStreamReader(new GZIPInputStream(fis)));
        else
            br = new BufferedReader(new InputStreamReader(fis));

        int count = 0;
        String line;
        while ((line = br.readLine()) != null) {
            try {
                String [] fields = line.split("\\|", -1);
                // idpv4
                //LogEntry l = new LogEntry (fields[1], fields[3], fields[4]);
                // idpv3
                LogEntry l = new LogEntry (fields[0], fields[8], fields[3]);
                if (logRepo.findById(l.getIndex()).isEmpty()) {
                    logRepo.save(l);
                    count++;
                }
            }
            catch (Exception e) {
                System.out.println ("Failed to read log: "+ line + " - " + e.getMessage());
            }
        }
        br.close();
        fis.close();
            
        return count;
    }

    Collection <String> getLogfiles (String directory) {
        
        File folder = new File (directory);
        if (folder.exists() && folder.isDirectory()) {
            Collection <String> fileNames = new HashSet <String> ();
            for (File f : folder.listFiles())
                fileNames.add(f.getAbsolutePath());
            return fileNames;            
        }    
        return null;
    }

    public String getLatestLogTime () {
        LogEntry le = logRepo.getLatestLog();
        if (le.getTimeStamp() != null)
            return le.getTimeStamp();
        else
            return null;
    }

    public int importLogs (boolean onlyImportNew) throws IOException {
        int logCount = 0;
        String timestamp = "0";
        if (onlyImportNew)
            timestamp = getLatestLogTime ();

        for (String logDir : logDirectories) 
            for (String logFile : getLogfiles(logDir)) 
                //if (logFile.matches(regex))...
                // check to see whether file is sufficiently recent...
                logCount += readLogfile(logFile);

        return logCount;
    }
}
