package mash.pies.reporting.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.saml2.Saml2LoginConfigurer;
import org.springframework.security.core.AuthenticatedPrincipal;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.saml2.provider.service.authentication.DefaultSaml2AuthenticatedPrincipal;
import org.springframework.security.saml2.provider.service.authentication.Saml2Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
class Saml2LoginSettings implements Customizer <Saml2LoginConfigurer<HttpSecurity>> {

    @Override
    public void customize(Saml2LoginConfigurer<HttpSecurity> t) {
   
        t.successHandler(new SavedRequestAwareAuthenticationSuccessHandler() {

            @Override
            public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                    Authentication authentication) throws IOException, ServletException {

                
                authentication = assignAuthorities (authentication, request);
                super.onAuthenticationSuccess(request, response, authentication);
            }
        }); 
    }

    /** 
     * Specifically checking for eduPersonEntitlement contains 'shibReporter'
     */
    private Saml2Authentication assignAuthorities (Authentication authentication, HttpServletRequest request) {

        Saml2Authentication sAuth = (Saml2Authentication) authentication;

        DefaultSaml2AuthenticatedPrincipal princ = (DefaultSaml2AuthenticatedPrincipal) authentication.getPrincipal();

        if (princ.getAttribute("urn:oid:1.3.6.1.4.1.5923.1.1.1.7").contains("urn:mace:dir:entitlement:shibReporter")) {
            Collection<SimpleGrantedAuthority> currentAuthorities = (Collection<SimpleGrantedAuthority>)SecurityContextHolder.getContext()
                .getAuthentication().getAuthorities();
            List<SimpleGrantedAuthority> updatedAuthorities = new ArrayList<SimpleGrantedAuthority>();
            updatedAuthorities.addAll(currentAuthorities);
            SimpleGrantedAuthority authority = new SimpleGrantedAuthority("REPORTS");
            updatedAuthorities.add(authority);

            sAuth = new Saml2Authentication(
                    (AuthenticatedPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal(),
                    sAuth.getSaml2Response(),
                    updatedAuthorities
            );
        
            SecurityContextHolder.getContext().setAuthentication(sAuth);
        }
        return sAuth;
    }
}
