package mash.pies.reporting.server.chartUtils;

import com.google.gson.JsonArray;

public class CountDataSet extends DataSet <Integer> {

    public void increment (Key key) {

        Integer currentValue = this.get(key);
        
        if (currentValue == null)
            currentValue = 0;

        this.set(key, ++currentValue);
    }
    
    public JsonArray getData () {
        return this.getData(0);
    }
}
