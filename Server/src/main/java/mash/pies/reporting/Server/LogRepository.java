package mash.pies.reporting.server;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface LogRepository extends CrudRepository <LogRepository.LogEntry, LogRepository.LogEntryIndex> {

    @Query (
            value="SELECT timestamp FROM logentry le WHERE le.timestamp = MAX (SELECT timestamp FROM logentry)",
            nativeQuery=true
    )
    public LogEntry getLatestLog ();

    @Query(value="(SELECT COUNT(*) from logentry", nativeQuery=true)
    public Integer getLogCount();

    @Entity
    @Table(name="logEntry")
    public static class LogEntry {

        @EmbeddedId 
        private LogEntryIndex index;

        protected LogEntry () {}

        public LogEntry (String timestamp, String username, String sp) throws Exception {
            if (!timestamp.startsWith("20"))
                throw new Exception ("Invalid timestamp in logentry details: timestamp: " + timestamp + "; username: " + username + "; sp: " + sp);
            if (!username.matches("\\w*"))
                throw new Exception ("Invalid username in logentry details: timestamp: " + timestamp + "; username: " + username + "; sp: " + sp);
            if (!sp.startsWith("http"))
                throw new Exception ("Invalid sp entityID logentry details: timestamp: " + timestamp + "; username: " + username + "; sp: " + sp);
            this.index = new LogEntryIndex(timestamp, username, sp);
        }

        public String getTimeStamp () {return index.getTimeStamp();}
        public String getUsername () {return index.getUsername();}
        public String getServiceProvider () {return index.getServiceProvider();}    
        
        public LogEntryIndex getIndex () {return index;}

        public String toString () {
            return getIndex().toString();
        }
    }

    @Embeddable
    public static class LogEntryIndex implements Serializable {

        private String timeStamp;
        private String username;
        private String sp;

        protected LogEntryIndex () {}

        public LogEntryIndex (String timeStamp, String username, String sp) {
            this.timeStamp = timeStamp;
            this.username = username;
            this.sp = sp;
        }

        public String getTimeStamp () {return this.timeStamp;}
        public String getUsername () {return this.username;}
        public String getServiceProvider () {return this.sp;}   

        @Override
        public String toString () {
            return getTimeStamp() + ":" + getServiceProvider() + ":" + getUsername();
        }     
    }
    
}
